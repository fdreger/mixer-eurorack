$fa = 1;
$fs = 0.4;

panelThickness = 2;
panelOuterHeight = 128.5;
hp=5.08;
mountHoleDiameter = 3.2;
holeWidth = 3.2;
offsetToMountHoleCenterY=3;
offsetToMountHoleCenterX = 7.5;


module eurorackPanel(panelHp,  mountHoles=2)
{
    
    hw = holeWidth;
    doepferWidths = [
        [1      ,     5     ],
        [1.5    ,     7.50  ],
        [2      ,     9.80  ],
        [4      ,     20    ],
        [6      ,     30    ],
        [8      ,     40.30 ],
        [10     ,     50.50 ],
        [12     ,     60.60 ],
        [14     ,     70.80 ],
        [16     ,     80.90 ],
        [18     ,     91.30 ],
        [20     ,     101.30],
        [21     ,     106.30],
        [22     ,     111.40],
        [28     ,     141.90],
        [42     ,     213.00],
    ];
    
    panelWidth = search(panelHp, doepferWidths);
    // panelWidth = hp*panelHp; // non-doepfer-compliant calculation
    
    //mountHoles ought to be even. Odd values are -=1
    difference()
    {
        cube([hp*panelHp,panelOuterHeight,panelThickness]);
        
        eurorackMountHoles(panelHp, mountHoles);
        
        for (i = [1,3,4,5,6]) {
            translate([0, i * (panelOuterHeight / 7.5), 0]) {
                step = hp*panelHp / 7;
                translate([step * 2, 0, 0]) jack();
                translate([step * 5, 0, 0]) alphaPot();
            }
        }
    }
}

module mixerPanel() {
    difference() {
        eurorackPanel(8, 4);
    }
}

module jack() {
    hole(6.2);
}

module alphaPot() {
    hole(7.2);
}

module led() {
    hole(4);
}

module usbA() {
     translate([-1 * 5.5 / 2,-1 * 12.5 / 2,-1]) cube([5.5, 12.5, panelThickness + 2]);
}


module hole(holeWidth) {
     translate([0,0,-1]) cylinder(r=holeWidth / 2, h=panelThickness + 2);
}

module eurorackMountHoles(php, hw)
{
    
    translate ([0, panelOuterHeight-offsetToMountHoleCenterY, 0])
        eurorackMountHolesRow(php, hw);

    translate ([0, offsetToMountHoleCenterY, 0])
    eurorackMountHolesRow(php, hw);
}

module eurorackMountHolesRow(php, hw)
{
    
    
    //topleft
    translate([offsetToMountHoleCenterX,0,0])
    {
        eurorackMountHole(hw);
        
        if(php >= 10)
        {
            translate([(hp*php) - (hp * 3), 0, 0])
            {
                eurorackMountHole(hw);
            }
        }
        
    }
}

module eurorackMountHole(hw)
{
    
    mountHoleRad = mountHoleDiameter/2;
    mountHoleDepth = panelThickness+2; //because diffs need to be larger than the object they are being diffed from for ideal BSP operations
    
    translate([0,0,-1]){
    union()
    {
        cylinder(r=mountHoleRad, h=mountHoleDepth, $fn=20);
    }
}
}


module cylinder_outer(height,radius,$fn=20){
   fudge = 1/cos(180/fn);
   cylinder(h=height,r=radius*fudge,$fn=$fn);
    }

//Samples
//eurorackPanel(4, 2,holeWidth);
projection(cut=false)
    mixerPanel();
//eurorackPanel(60, 8,holeWidth);